package com.skava.reusable.components;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.SkipException;

import com.framework.reporting.BaseClass;
import com.framework.reporting.ExtentTestManager;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.relevantcodes.extentreports.LogStatus;
import com.skava.frameworkutils.ExcelUtils;
import com.skava.frameworkutils.loggerUtils;
import com.skava.networkutils.ApiReaderUtils;


public class GeneralComponents extends BaseClass {

	String authToken="";
	String storeid="";
	protected String storeType="";
	public static long proccessedTime;
	String createdSalesCatalog = "";
	public static String ce  = "true";
	public static String skuAssociationSample = "";
	public static Map<String,String> foundationDetails = new HashMap<String,String>();
	public static Map<String,String> existingBusinessDetails = new LinkedHashMap<String,String>();
	public static Map<String,String> eb_catalog_product_data = new LinkedHashMap<String,String>();
	public static Map<String,String> eb_catalog_sku_data = new LinkedHashMap<String,String>();
	public static Multimap<String, String> eb_category_data = ArrayListMultimap.create();
	public static Multimap<String, String> eb_price_data = ArrayListMultimap.create();
	protected String CurrentUserName = "";
	protected String CurrentPassword = "";
	protected String CurrentName = "";
	protected String plpKey="";
	String productPageIdentifier = "";
	public HashMap<String, String> promoDetailsForReact = new HashMap<String, String>();
	
	public void logPass(String message)
	{
		System.out.println(message);
		ExtentTestManager.getTest().log(LogStatus.PASS, message);
	}

	public void logInfo(String message)
	{
		System.out.println(message);
		ExtentTestManager.getTest().log(LogStatus.INFO, message);
	}
	public void logFail(String message)
	{
		System.out.println(message);
		ExtentTestManager.getTest().log(LogStatus.INFO,driver.getCurrentUrl());
		ExtentTestManager.getTest().log(LogStatus.FAIL, message);
		loggerUtils.failLog(message+" is mismatched");
		loggerUtils.failLog("#### Failed in URL : " +driver.getCurrentUrl());
		System.out.println("#### Failed in URL : " +driver.getCurrentUrl());
		if(properties.getProperty("screenshots").equals("true"))
		{
			String scrFile = driver.takeScreenShotReturnFilePath();
			ExtentTestManager.getTest().log(LogStatus.FAIL,ExtentTestManager.getTest().addBase64ScreenShot(scrFile));
		}
	}
	

	
	
	
	
		}
