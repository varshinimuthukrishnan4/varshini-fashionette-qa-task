package com.skava.reusable.components;

import com.skava.object.repository.Login;
import com.skava.object.repository.addtocart;

public class ProductListingPage extends UserdetailsPage {
	String pdtnameaddedbefore;

	public void NavigatePLP() {
		/*
		 * driver.explicitWaitforVisibility(Login.bannercontent, "banner content");
		 * driver.jsClickElement(Login.bannercontent, "popclick");
		 * driver.jsClickElement(Login.bannertext, "bannertextinside");
		 * driver.jsClickElement(Login.banneraccept, "banneraccept");
		 */
		driver.jsClickElement(Login.stagewrapbutton, "closebanner");
		logPass("Navigate to PLP Page By Clicking Discover Now Button");
		driver.scrollToElement(addtocart.plpproduct1, "");
		logPass("Landed to the PLP Page is done successfully");
		driver.explicitWaitforVisibility(addtocart.plpproduct1, "");
		driver.jsClickElement(addtocart.plpproduct1, "pdp page");
		logPass("Navigation to PLP is Successful");

	}

	public void AddtoCart() {
		driver.explicitWaitforVisibility(addtocart.pdpproductdesc, "titleoftheproduct");
		logPass("Navigate to Pdp Page is SuccessFull");
		driver.jsClickElement(addtocart.pdpproductdesc, "titleoftheproduct");
		logPass("Product is clicked successfully");
		driver.explicitWaitforVisibility(addtocart.addtocart, "addtocartbuttonisclicked");
		driver.jsClickElement(addtocart.addtocart, "addtocartbuttonisclicked");
		logPass("AddtoCart is done successfully");
		driver.explicitWaitforVisibility(addtocart.pdpdescription, "Productdescription");
		pdtnameaddedbefore = driver.getText(addtocart.pdpdescription, "Productdescription");
		System.out.println("Name of the Product added is" + pdtnameaddedbefore);
		logPass("The Name of the Producted selected In PLP Page is ->:" + pdtnameaddedbefore);

	}

	public void cartvalidation() {

		driver.explicitWaitforVisibility(addtocart.welcometext, "welcometext");
		driver.actionClickElement(addtocart.welcometext, "welcometext");
		logFail("Cart Page Navigation Begins ");
		driver.explicitWaitforVisibility(addtocart.addtocart1, "Click the addtocartbutton");
		driver.actionClickElement(addtocart.addtocart1, "Click the addtocartbutton");
		logPass("AddtoCart Button is Clicked");
		driver.explicitWaitforClickable(addtocart.cartelementvalidate, "element to be validated");
		String pdtaddedincart = driver.getText(addtocart.cartelementvalidate, "element to be validated");
		System.out.println("Name of the Product added in the cartpage is->" + pdtaddedincart);
		
		logPass("Name of the Product is Inside the Cart Page" + pdtaddedincart);
		if (pdtnameaddedbefore.equalsIgnoreCase(pdtaddedincart)) {
			System.out.println("Product is validated inside cart");
			logPass("CartValidation is done successfully");
		} else {
			System.out.println("Product is not validated inside cart");
			logFail("Cart Validation Failed");
		}
	}

	public void ClearCart() {
		driver.explicitWaitforVisibility(addtocart.addtocartbuttonvalidate1, "addtocartbuttonclick");
		driver.jsClickElement(addtocart.addtocartbuttonvalidate1, "addtocartbuttonclick");
		driver.actionClickElement(addtocart.clearcart, "addeditemiscleared");

	}
}
