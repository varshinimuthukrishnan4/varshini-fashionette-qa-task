package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.HomePageComponents;

public class Scenario3 extends HomePageComponents {
	@BeforeClass(alwaysRun = true)
	public void initTest() throws IOException {
		driver = initiTest(this.getClass().getSimpleName());
		driver.maximizeBrowser();
	}

	@Test
	public void Scenario3() throws Exception {
   
     launchUrl();
     NavigatePLP();
     AddtoCart();
     applyvocher();
     
	}
	
	
	@AfterClass(alwaysRun = true)
	public synchronized void tearDown() {
		if (driver != null) 
		{
			//driver.quit();
		}
	}
}
